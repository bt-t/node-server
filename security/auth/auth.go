package auth

import (
	"errors"
	"regexp"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/bt-t/node-server/databases"
	"gitlab.com/bt-t/node-server/security/hashing"
)

//Function takes node's id and token, hashes token and checks against db
func CheckToken(id, token string) (bool, error) {

	//TODO: Sanitize inpu length
	//if len(id) != 32 || len(token) != 32 {
	//	return false, errors.New("INVALID token or id length")
	//}

	validId, err := CheckId(id)
	if !validId {
		return false, err
	}

	//get node entry from database

	rows, err := (&databases.PqDb).Query(`SELECT tokenhash FROM Nodes WHERE id=$1;`, id)
	if err != nil {
		log.Error(err)
		return false, errors.New("ERROR while retreaving data from db")
	}

	defer rows.Close()

	var tokenhash string
	var count int
	for rows.Next() {
		count++
		err = rows.Scan(&tokenhash)
	}

	if count != 1 {
		return false, errors.New("NODE does not exist")
	}

	if err != nil {
		return false, err
	}

	//Decode argon2 string
	fields := strings.Split(tokenhash, "$")
	salt := fields[4]

	//hash token passed into function using salt from db
	newhash, err := hashing.ArgonHashSalted(token, salt)
	if err != nil {
		return false, errors.New("HASHING unsuccessful")
	}

	//Return
	return tokenhash == newhash, nil
}

func CheckId(id string) (bool, error) {
	//Check if id is compliant with id guidelines
	regex := `[A-Za-z0-9]+`
	match, err := regexp.Match(regex, []byte(id))
	if err != nil {
		log.Error(err)
		return false, errors.New("ERROR while checking id validity")
	}

	return match, nil
}
