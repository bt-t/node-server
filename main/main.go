package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
	"gitlab.com/bt-t/node-server/databases"
	"gitlab.com/bt-t/node-server/routes"
)

//Stores data from config file (read at app start)
var config = make(map[string]string)

// Map that stores all active websockets, used to rate-limit connected,
// map is used to reduce the time complexity to O(1), the integer stores the unix epoch of the last request
var activeSockets = make(map[*websocket.Conn]int64)

func main() {

	log.Info("Reading config...")
	//Read and parse config file
	config = readCfg("./config.json")

	log.Info("Connecting to primary db...")
	err := databases.PqInit(config["db_driver"], config["postgres_uri"])
	if err != nil {
		log.Fatal(err)
	}
	log.Info("Connected to primary db!")

	err = databases.RedisInit(config["redis_uri"])
	if err != nil {
		log.Fatal(err)
	}

	log.Info("Starting http-server...")
	i, _ := strconv.Atoi(config["rate_limit_sec"])
	http.HandleFunc("/ws", routes.SocketHandler(&activeSockets, i))

	log.Fatal(http.ListenAndServe(config["serve"], nil))
}

//Function to read data from ./config.json
func readCfg(file string) map[string]string {

	var data = make(map[string]string)

	//read config.json
	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal("Error when opening file: ", err)
	}

	//Rarse contents of config.json
	if err := json.Unmarshal(content, &data); err != nil {
		log.Fatal(err)
	}

	return data
}
