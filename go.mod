module gitlab.com/bt-t/node-server

go 1.17

require (
	github.com/go-redis/redis/v8 v8.11.4
	github.com/gorilla/websocket v1.4.2
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	github.com/lib/pq v1.10.4
)

require (
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/sys v0.0.0-20211113001501-0c823b97ae02 // indirect
)
