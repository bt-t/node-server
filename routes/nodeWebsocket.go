package routes

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
	"gitlab.com/bt-t/node-server/databases"
	"gitlab.com/bt-t/node-server/security/auth"
)

//Func that handles the creation of new websockets, wrapped to accept aditional inputs
func SocketHandler(as *map[*websocket.Conn]int64, rateLimitSec int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		var upgrader = websocket.Upgrader{} // use default options

		//Read Bearer token from header
		cid := r.Header.Get("Client-ID")
		ctk := r.Header.Get("Authorization")

		if cid == "" || ctk == "" { // Send error if bearer auth is incomplete
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		authorized, err := auth.CheckToken(cid, ctk)

		if err != nil || !authorized {
			w.WriteHeader(http.StatusUnauthorized)
			log.Warn("Unauthorized node tried to connect: ", err)
			return
		}

		upgrader.CheckOrigin = func(r *http.Request) bool { return true }
		ws, err := upgrader.Upgrade(w, r, nil)

		if err != nil {
			log.Error("Error while upgrading websocket:", err)
			return
		}

		log.Info("Client connected successfully!")
		(*as)[ws] = 0                // Add new socket to map, bool does not do anything, only used to make map possible
		reader(ws, as, rateLimitSec) // Call reader function to enable reading & logic on the passed ws
	}
}

// Function that listens for messages on a specific ws and handles logic
func reader(conn *websocket.Conn, as *map[*websocket.Conn]int64, rateLimitSec int) {

	type devices struct {
		Bt  []string `json:"bt"`
		Ble []string `json:"ble"`
	}

	//Struct data provided by node has to follow
	//Auth is not required here (https://stackoverflow.com/a/32619655)
	type MsgJson struct {
		Id   string  `json:"id"`   //Node's id
		Type string  `json:"type"` //type of request
		Data devices `json:"data"`
	}

	for { // listen for ws calls for infinity
		messageType, message, err := conn.ReadMessage()

		if err != nil { // handle errors

			log.Error("Error while reading ws message, removing device:", err.Error())
			//remove websocket from map
			delete((*as), conn)
			log.Info("Remaining open sockets: ", len((*as)))
			//stop execution loop
			break
		}

		// parse json sent by node
		var jsonmsg MsgJson
		err = json.Unmarshal(message, &jsonmsg)
		if err != nil {
			log.Error("Error during json-parsing of node's message:", err)
			sendWsError(conn, messageType, wserror{"Invalid json"})
			break
		}

		validId, _ := auth.CheckId(jsonmsg.Id)

		if !validId {
			log.Error("INVALID id provided by node")
			break
		}

		//RATE LIMITING
		//Check if time difference between last scan and current time is within spec
		if (time.Now().Unix() - (*as)[conn]) < int64(rateLimitSec) {
			log.Warn("Rate limited")
			sendWsError(conn, messageType, wserror{"Rate limited"})

			//Set last submit time of node in allSockets map
			(*as)[conn] = time.Now().Unix()

		} else {

			//Set last submit time of node in allSockets map
			(*as)[conn] = time.Now().Unix()

			//Use data if not sent too fast

			switch jsonmsg.Type {
			case "measurement":

				log.Info("Node ", jsonmsg.Id, " submitted measurement data")

				var devices []string

				if len(jsonmsg.Data.Bt) > 0 {
					devices = append(devices, jsonmsg.Data.Bt...) //add standard bt devices to array
				}

				if len(jsonmsg.Data.Ble) > 0 {
					devices = append(devices, jsonmsg.Data.Ble...) //add ble devices to array
				}

				//set raw node data
				err = databases.RedSet("node_raw:"+jsonmsg.Id, devices)

				log.Info("node_raw:" + jsonmsg.Id)
				if err != nil {
					log.Error(err)
					return
				}

			default:
				log.Error("Invalid Operation sent by node")
				sendWsError(conn, messageType, wserror{"Invalid operation"})
			}
		}

	}
}

type wserror struct {
	Message string `json:"message"`
}

// Function to simplify sending a json-formatted error messages to websockets
func sendWsError(conn *websocket.Conn, messageType int, error wserror) {
	jsonTxt, err := json.Marshal(error)
	if err != nil {
		log.Error("Error while sending error to node [Ironic error]:", err)
	}
	err = conn.WriteMessage(messageType, jsonTxt)
	if err != nil {
		log.Error("Error while sending error to node [Ironic error]:", err)
	}
}
