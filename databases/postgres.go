package databases

import (
	"database/sql"

	_ "github.com/lib/pq"
)

var PqDb = sql.DB{}

func PqInit(dbDriver, conStr string) error {
	//connect to db server
	db, err := sql.Open(dbDriver, conStr)
	if err != nil {
		return err
	}
	PqDb = *db

	//ping db
	err = (&PqDb).Ping()
	return err
}
