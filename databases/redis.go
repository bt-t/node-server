package databases

import (
	"context"

	"github.com/go-redis/redis/v8"
	log "github.com/sirupsen/logrus"
)

var redisCtx = context.Background()

//Empty db connection, is used in redisInit()
var rdb *redis.Client

func RedisInit(uri string) error {

	log.Info("Connectiong to redis-db...")

	opt, err := redis.ParseURL(uri)
	if err != nil {
		return err
	}

	rdb = redis.NewClient(opt)

	log.Info("Testing connection...")

	err = rdb.Set(redisCtx, "test", "test", 0).Err()
	if err != nil {
		return err
	}

	_, err = rdb.Get(redisCtx, "test").Result()
	if err != nil {
		return err
	}

	log.Info("Connected!")

	return nil

}

//sets "values" as values for a redis set. Deletes data not prewsent in "values" from db
func RedSet(key string, values []string) error {

	if len(values) > 0 {
		//Get current contents of the set
		data, err := rdb.SUnion(redisCtx, key).Result()

		if err != nil {
			return err
		}

		//Delete all fields, that are no longer present
		var toDel []string
		for i := 0; i < len(data); i++ {
			if !checkDupe(values, data[i]) {
				toDel = append(toDel, data[i])
			}
		}

		if len(toDel) > 0 {
			_, err = rdb.SRem(redisCtx, key, toDel).Result()

			if err != nil {
				return err
			}
		}

		//Push new data to redis
		return rdb.SAdd(redisCtx, key, values).Err()
	} else {
		//delete set if no data is present
		return rdb.Del(redisCtx, key).Err()
	}

}

//returns true if values contains data
func checkDupe(values []string, data string) bool {

	for j := 0; j < len(values); j++ {
		if data == values[j] {
			return true
		}
	}
	return false
}
